# JARGON #



### What is JARGON? ###

**JARGON** (**J**udiciously **A**nalyzed **R**eport **G**enerated **O**n **N**atural Language Understanding) is an app 
that takes a document and converts its specialized terminology and concepts into a simplified interactive summarization 
with hyperlinks; taxonomic classification; grammatical parsing; semantic & syntactic analysis; relation extraction; and sentiment analysis.